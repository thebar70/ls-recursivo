#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <dirent.h>
#include <string.h>
#include <errno.h>

void listar(int pos, char *ruta );
char * reemplazarEspacios(char * nuevo_dir);
void stchcat(char *cadena, char chr);

struct struc_sub_directorios
{
    char ruta_subdir[50];
}typedef struc_sub_directorios;



//Implementacion de Funciones
void listar(int pos, char *ruta){
    
    struct dirent *fichero;
    DIR * dir;
    dir=opendir(ruta);
    struc_sub_directorios sub_directorios[5];
    
    if(dir == NULL){
       printf("Por aqui vienen los problemas: %s\n",strerror(errno));
    }
    printf("Directorio:  %s\n",ruta);
    char *p_ruta_completa;
    char *p_nuevo_dir;
    char *p_ruta_sin_espacios;
   
    char var_ruta_completa;
    char var_nuevo_dir;
    char var_ruta_sin_espacios;
   
    p_nuevo_dir=&var_nuevo_dir;
    p_ruta_completa=&var_ruta_completa;
    p_ruta_sin_espacios=&var_ruta_sin_espacios;
    int x=0;
    while((fichero = readdir(dir)) != NULL){
        
        /*Si el archivo leido es de tipo directorio se llama recursivamente*/
        if(fichero->d_type == DT_DIR){
            
            if( !strcmp(fichero->d_name, ".")) continue;
            if( !strcmp(fichero->d_name, "..")) continue;
            
            strcpy(p_nuevo_dir,fichero->d_name); 
            //Se concatena el fichero del directorio actual con el nuevo directorio
            printf("equis %d\n",x);
            strcat(p_nuevo_dir,"/");
            
            //se habre el nuevo directorio
            p_ruta_completa=ruta;
            
            // strcat(p_ruta_completa,p_nuevo_dir);
            //strcpy(p_ruta_sin_espacios,reemplazarEspacios(p_nuevo_dir)); Tambien da problemas
            
            p_ruta_sin_espacios=reemplazarEspacios(p_nuevo_dir);
            
            strcat(p_ruta_completa,p_ruta_sin_espacios);
            
            
            strcpy(sub_directorios[pos].ruta_subdir,p_ruta_completa);
            printf("\t\tDIR: %s\n",sub_directorios[pos].ruta_subdir);
            pos++;
        
        }else{
            printf("\t \t %s\n",fichero->d_name);
        }
    }
   
    //Hasta aqui va el while
    closedir(dir);
    printf("Sali\n");
    for(int i = 0; i < 10; i++)
    {
        listar(0,sub_directorios[i].ruta_subdir);
    }
    

    //llamamos a la funcion recursivamnete
    //listar(tab,ruta_completa); 
}


char * reemplazarEspacios(char * nuevo_dir){
    int tam_original=strlen(nuevo_dir);
    char *directorio_final=malloc(tam_original+1);
    char *actual=malloc(1);;
    
    int flag=1;   //Util para el caso de que la distancia sea mas de dos espacios
    
    for(int i = 0; i < tam_original; i++)
    {   
        actual=nuevo_dir[i];
        if(actual == ' ' ){
            if(flag != 1){
                strcat(directorio_final,"\\ ");
                flag=1;
            }
        }else{
            stchcat(directorio_final,actual);
           // printf("el nuevo dir es: %s\n",directorio_final);
            flag=0;
        }
    }
    //free(actual);
    // printf("dir final  %s\n: ",directorio_final);
    return directorio_final;
}

//Se agrega el nuevo char al final 
void stchcat(char *cadena, char chr){
   size_t longitud = strlen(cadena);

   *(cadena + longitud) = chr;
   *(cadena + longitud + 1) = '\0';
}

int main(int argc, char * argv[]){
    
    listar(0,argv[1]);
    exit(EXIT_SUCCESS);
}