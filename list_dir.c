#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <dirent.h>
#include <string.h>
#include <errno.h>


void listar(char *ruta );

struct struc_sub_directorios
{
    char ruta_subdir[100];
}typedef struc_sub_directorios;



//Implementacion de Funciones
void listar(char *ruta){
    
    struct dirent *fichero;
    DIR * dir;
    dir=opendir(ruta);
    struc_sub_directorios sub_directorios[20];
    char *nuevo_dir;
    int pos=0;
    int fila=0;
    
    printf("\n\t%s\n",ruta);
    while((fichero = readdir(dir)) != NULL){
        if( !strcmp(fichero->d_name, ".")) continue;
        if( !strcmp(fichero->d_name, "..")) continue;

        if(fichero->d_type==DT_DIR){
            printf("\033[0;34m\t%s",fichero->d_name);
            printf("\033[0m");
            nuevo_dir=(char *) malloc( strlen(fichero->d_name) +strlen(ruta) +1 );
            strcpy(nuevo_dir,ruta);
            strcat(nuevo_dir,fichero->d_name);
            strcat(nuevo_dir,"/");
            strcpy(sub_directorios[pos].ruta_subdir,nuevo_dir);
            pos++;
            fila++;
        }else{
            printf("\t%s",fichero->d_name);
            fila++;
        }
        if(fila>=4){
            printf("\n");
            fila=0;
        }
        
        
    }
    closedir(dir);
    for(int i = 0; i < pos; i++){
        listar(nuevo_dir);
    }
    
}

int main(int argc, char * argv[]){
    
    listar(argv[1]);
    exit(EXIT_SUCCESS);
}